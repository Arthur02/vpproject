<?php
	/* a template for displaying the top bar */

	if( chariti_get_option('general', 'enable-top-bar', 'enable') == 'enable' ){

		$top_bar_width = chariti_get_option('general', 'top-bar-width', 'boxed');
		$top_bar_container_class = '';

		if( $top_bar_width == 'boxed' ){
			$top_bar_container_class = 'chariti-container ';
		}else if( $top_bar_width == 'custom' ){
			$top_bar_container_class = 'chariti-top-bar-custom-container ';
		}else{
			$top_bar_container_class = 'chariti-top-bar-full ';
		}

		$top_bar_menu = chariti_get_option('general', 'top-bar-menu-position', 'none');
		$top_bar_social = chariti_get_option('general', 'enable-top-bar-social', 'enable');
		$top_bar_social_position = chariti_get_option('general', 'top-bar-social-position', 'right');

		echo '<div class="chariti-top-bar" >';
		echo '<div class="chariti-top-bar-background" ></div>';
		echo '<div class="chariti-top-bar-container ' . esc_attr($top_bar_container_class) . '" >';
		echo '<div class="chariti-top-bar-container-inner clearfix" >';

		$language_flag = chariti_get_wpml_flag();
		$left_text = chariti_get_option('general', 'top-bar-left-text', '');
		if( !empty($left_text) || !empty($language_flag) || ($top_bar_menu == 'left' && has_nav_menu('top_bar_menu')) || 
			($top_bar_social == 'enable' && $top_bar_social_position == 'left')  ){

			echo '<div class="chariti-top-bar-left chariti-item-pdlr">';
		
			// social
			if( $top_bar_social == 'enable' && $top_bar_social_position == 'left' ){
				echo '<div class="chariti-top-bar-right-social" >';
				get_template_part('header/header', 'social');
				echo '</div>';	

				$top_bar_social = 'disable';
			}

			echo '<div class="chariti-top-bar-left-text" >';
			if( $top_bar_menu == 'left' ){
				chariti_get_top_bar_menu('left');
			}
			echo gdlr_core_escape_content($language_flag);
			echo gdlr_core_escape_content(gdlr_core_text_filter($left_text));
			echo '</div>';

			echo '</div>'; // chariti-top-bar-left
		}

		echo '<div class="chariti-top-bar-right chariti-item-pdlr">';
		if( $top_bar_menu == 'right' && has_nav_menu('top_bar_menu') ){
			chariti_get_top_bar_menu('right');
		}

		$right_text = chariti_get_option('general', 'top-bar-right-text', '');
		if( !empty($right_text) ){
			echo '<div class="chariti-top-bar-right-text">';
			echo gdlr_core_escape_content(gdlr_core_text_filter($right_text));
			echo '</div>';
		}

		// social
		if( $top_bar_social == 'enable' && $top_bar_social_position == 'right' ){
			echo '<div class="chariti-top-bar-right-social" >';
			get_template_part('header/header', 'social');
			echo '</div>';	
		}

		$top_bar_login = chariti_get_option('general', 'enable-top-bar-right-login', 'enable');
		if( $top_bar_login == 'enable' && function_exists('frmaster_user_top_bar') ){
			echo frmaster_user_top_bar();
		}
		echo '</div>'; // chariti-top-bar-right

		echo '</div>'; // chariti-top-bar-container-inner
		echo '</div>'; // chariti-top-bar-container
		echo '</div>'; // chariti-top-bar

	}  // top bar
?>