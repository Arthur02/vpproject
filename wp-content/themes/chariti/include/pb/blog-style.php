<?php

	add_filter('gdlr_core_blog_info_prefix', 'chariti_gdlr_core_blog_info_prefix');
	if( !function_exists('chariti_gdlr_core_blog_info_prefix') ){
		function chariti_gdlr_core_blog_info_prefix(){

			$font_icons = chariti_get_option('plugin', 'font-icon', array());
			if( in_array('simpleline', $font_icons) ){
				$date = '<i class="icon-clock" ></i>';
			}else{
				$date = '<i class="fa fa-clock-o" ></i>';
			}
			return array(
				'date' => $date,
				'tag' => '<i class="fa fa-tag" ></i>',
				'category' => '<i class="fa fa-folder-o" ></i>',
				'comment' => '<i class="fa fa-comments-o" ></i>',
				'like' => '<i class="icon_heart_alt" ></i>',
				'author' => '<i class="fa fa-user" ></i>',
				'comment-number' => '<i class="fa fa-comments-o" ></i>',
			);
		}
	}

	add_filter('gdlr_core_blog_style_content', 'chariti_gdlr_core_blog_style_content', 10, 3);
	if( !function_exists('chariti_gdlr_core_blog_style_content') ){
		function chariti_gdlr_core_blog_style_content( $ret, $args, $blog_style ){

			if( in_array($args['blog-style'], array('blog-image', 'blog-image-no-space')) ){
				return chariti_blog_modern_style_4($args, $blog_style);
			}else if( in_array($args['blog-style'], array('blog-column', 'blog-column-no-space', 'blog-column-with-frame')) ){
				if( empty($args['blog-column-style']) || $args['blog-column-style'] == 'style-1' ){
					return chariti_blog_grid_style_1($args, $blog_style);
				}
			}

		}
	}

	if( !function_exists('chariti_blog_grid_style_1') ){
		function chariti_blog_grid_style_1( $args, $blog_style ){

			$post_format = get_post_format();
			if( in_array($post_format, array('aside', 'quote', 'link')) ){
				$args['extra-class']  = ' gdlr-core-blog-grid gdlr-core-small';
				if($args['blog-style'] == 'blog-column-with-frame' && $args['layout'] != 'masonry' ){
					$args['sync-height'] = true;
				}
				return $blog_style->blog_format( $args, $post_format );
			}

			$additional_class = '';

			// shadow
			$blog_atts = array(
				'margin-bottom' => empty($args['margin-bottom'])? '': $args['margin-bottom']
			);
			$thumbnail_shadow = array();
			if($args['blog-style'] == 'blog-column-with-frame' ){
				$additional_class .= ' gdlr-core-blog-grid-with-frame gdlr-core-item-mgb gdlr-core-skin-e-background ';

				$blog_atts['border-width'] = empty($args['blog-frame-border-size'])? '': $args['blog-frame-border-size'];
				$blog_atts['border-color'] = empty($args['blog-frame-border-color'])? '': $args['blog-frame-border-color'];

				if( !empty($args['frame-shadow-size']['size']) && !empty($args['frame-shadow-color']) && !empty($args['frame-shadow-opacity']) ){
					$blog_atts['background-shadow-size'] = $args['frame-shadow-size'];
					$blog_atts['background-shadow-color'] = $args['frame-shadow-color'];
					$blog_atts['background-shadow-opacity'] = $args['frame-shadow-opacity'];
					$blog_atts['border-radius'] = empty($args['blog-border-radius'])? '': $args['blog-border-radius'];

					$additional_class .= ' gdlr-core-outer-frame-element';
				}
			}else{
				if( !empty($args['frame-shadow-size']['size']) && !empty($args['frame-shadow-color']) && !empty($args['frame-shadow-opacity']) ){
					$thumbnail_shadow['background-shadow-size'] = $args['frame-shadow-size'];
					$thumbnail_shadow['background-shadow-color'] = $args['frame-shadow-color'];
					$thumbnail_shadow['background-shadow-opacity'] = $args['frame-shadow-opacity'];
				}
				$thumbnail_shadow['border-radius'] = empty($args['blog-border-radius'])? '': $args['blog-border-radius'];
			}

			// move up with shadow effect
			if( $args['blog-style'] == 'blog-column-with-frame' && !empty($args['enable-move-up-shadow-effect']) && $args['enable-move-up-shadow-effect'] == 'enable' ){
				$additional_class .= ' gdlr-core-move-up-with-shadow gdlr-core-outer-frame-element';
			}

			if($args['blog-style'] == 'blog-column-with-frame' && $args['layout'] != 'masonry' ){
				$ret  = '<div class="gdlr-core-blog-grid gdlr-core-js ' . esc_attr($additional_class) . '" ' . gdlr_core_esc_style($blog_atts);
				$ret .= ' data-sync-height="blog-item-' . esc_attr($blog_style->blog_item_id) . '" >';
			}else{
				$ret  = '<div class="gdlr-core-blog-grid ' . esc_attr($additional_class) . '" ' . gdlr_core_esc_style($blog_atts) . ' >';
			}
			if( empty($args['show-thumbnail']) || $args['show-thumbnail'] == 'enable' ){
				$ret .= $blog_style->blog_thumbnail(array(
					'thumbnail-size' => $args['thumbnail-size'],
					'post-format' => $post_format,
					'post-format-gallery' => 'slider',
					'opacity-on-hover' => empty($args['enable-thumbnail-opacity-on-hover'])? 'enable': $args['enable-thumbnail-opacity-on-hover'],
					'zoom-on-hover' => empty($args['enable-thumbnail-zoom-on-hover'])? 'enable': $args['enable-thumbnail-zoom-on-hover'],
					'grayscale-effect' => empty($args['enable-thumbnail-grayscale-effect'])? 'disable': $args['enable-thumbnail-grayscale-effect']
				), $thumbnail_shadow);
			}
			
			if( $args['blog-style'] == 'blog-column-with-frame' ){
				$ret .= '<div class="gdlr-core-blog-grid-frame" ' . gdlr_core_esc_style(array(
					'padding' => empty($args['blog-frame-padding'])? '': $args['blog-frame-padding'],
				)) . ' >';
			}else{
				$ret .= '<div class="gdlr-core-blog-grid-content-wrap">';
			}
			
			$ret .= $blog_style->blog_title($args);
			if( !empty($args['meta-option']) ){
				$ret .= '<div class="gdlr-core-blog-info-wrapper gdlr-core-skin-divider" >';
				$ret .= $blog_style->blog_info(array(
					'display' => $args['meta-option'],
					'wrapper' => false
				));
				$ret .= '</div>';
			}

			$ret .= $blog_style->get_blog_excerpt($args);
			$ret .= '</div>'; // gdlr-core-blog-grid-content-wrap
			$ret .= '</div>'; // gdlr-core-blog-grid
			
			return $ret;
		}
	}

	if( !function_exists('chariti_blog_modern_style_4') ){
		function chariti_blog_modern_style_4( $args, $blog_style ){
			if( $args['blog-image-style'] != 'style-4' ) return '';

			$feature_image = get_post_thumbnail_id();
			$additional_class  = empty($feature_image)? ' gdlr-core-no-image': ' gdlr-core-with-image';
			
			if( empty($args['always-show-overlay-content']) || $args['always-show-overlay-content'] == 'disable' ){
				$additional_class .= ' gdlr-core-hover-overlay-content';
			}
			if( empty($args['enable-thumbnail-opacity-on-hover']) || $args['enable-thumbnail-opacity-on-hover'] == 'enable' ){
				$additional_class .= ' gdlr-core-opacity-on-hover';
			}
			if( empty($args['enable-thumbnail-zoom-on-hover']) || $args['enable-thumbnail-zoom-on-hover'] == 'enable' ){
				$additional_class .= ' gdlr-core-zoom-on-hover';
			}
			if( !empty($args['enable-thumbnail-grayscale-effect']) && $args['enable-thumbnail-grayscale-effect'] == 'enable' ){
				$additional_class .= ' gdlr-core-grayscale-effect';
			}
			if( !empty($args['blog-image-thumbnail-overlay']) && $args['blog-image-thumbnail-overlay'] == 'gradient-slide' ){
				$additional_class .= ' gdlr-core-gradient-slide';
			}
			if( !empty($args['blog-image-style']) ){
				$additional_class .= ' gdlr-core-' . $args['blog-image-style'];
			}
			
			$thumbnail_shadow = array();
			if( !empty($feature_image) ){
				if( !empty($args['frame-shadow-size']['size']) && !empty($args['frame-shadow-color']) && !empty($args['frame-shadow-opacity']) ){
					$thumbnail_shadow['background-shadow-size'] = $args['frame-shadow-size'];
					$thumbnail_shadow['background-shadow-color'] = $args['frame-shadow-color'];
					$thumbnail_shadow['background-shadow-opacity'] = $args['frame-shadow-opacity'];
				}

				$thumbnail_shadow['border-radius'] = empty($args['blog-border-radius'])? '': $args['blog-border-radius'];

				$additional_class .= ' gdlr-core-outer-frame-element';
			}

			$ret  = '<div class="gdlr-core-blog-modern ' . esc_attr($additional_class) . '" ' . gdlr_core_esc_style($thumbnail_shadow) . ' >';
			$ret .= '<div class="gdlr-core-blog-modern-inner">';

			if( !empty($feature_image) ){
				$ret .= '<div class="gdlr-core-blog-thumbnail gdlr-core-media-image" >';
				$ret .= gdlr_core_get_image($feature_image, $args['thumbnail-size'], array(
					'placeholder' => false,
					'img-style' => gdlr_core_esc_style(array(
						'opacity' => empty($args['blog-image-initial-opacity'])? '': $args['blog-image-initial-opacity']
					))
				));
				if( !empty($args['blog-image-style']) && $args['blog-image-style'] == 'style-2' && in_array('category', $args['meta-option']) ){
					$ret .=  $blog_style->blog_info(array(
						'display' => array('category'),
						'category-background-color' => empty($args['category-background-color'])? '': $args['category-background-color']
					));
					$args['meta-option'] = array_diff($args['meta-option'], array('category'));
				}
				$ret .= '</div>';
			}
			
			if( !empty($args['blog-image-thumbnail-overlay']) ){
				if( $args['blog-image-thumbnail-overlay'] == 'gradient' ){
					$ret .= '<div class="gdlr-core-blog-modern-content-overlay-gradient" ></div>';
				}else if( $args['blog-image-thumbnail-overlay'] == 'gradient2' ){
					$ret .= '<div class="gdlr-core-blog-modern-content-overlay-gradient2" ></div>';
				}else if( $args['blog-image-thumbnail-overlay'] == 'opacity' ){
					$ret .= '<div class="gdlr-core-blog-modern-content-overlay" ' . gdlr_core_esc_style(array(
						'background-color' => (empty($args['blog-image-thumbnail-overlay-color']) || $args['blog-image-thumbnail-overlay-color'] == '#000000')? '': $args['blog-image-thumbnail-overlay-color'],
						'opacity' => (empty($args['blog-image-thumbnail-overlay-opacity']) || $args['blog-image-thumbnail-overlay-opacity'] == '0.4')? '': $args['blog-image-thumbnail-overlay-opacity']
					)) . ' ></div>';
				}
			}
			$ret .= '<div class="gdlr-core-blog-modern-content ';
			$ret .= empty($args['blog-image-alignment'])? ' gdlr-core-center-align': ' gdlr-core-' . esc_attr($args['blog-image-alignment']) . '-align';
			$ret .= '" ' . gdlr_core_esc_style(array(
				'padding' => empty($args['blog-image-overlay-content-padding'])? '': $args['blog-image-overlay-content-padding']
			)) . '>';
			$ret .= $blog_style->blog_title($args);

			foreach($args['meta-option'] as $key => $val){
				if( !in_array($val, array('date', 'category')) ){
					unset($args['meta-option'][$key]);
				}
			}
			$args['after-content'] = $blog_style->blog_info(array(
				'display' => $args['meta-option'],
				'wrapper' => true
			));
			if( !empty($args['blog-image-excerpt-number']) ){
				$args['excerpt'] = 'specify-number';
				$args['excerpt-number'] = $args['blog-image-excerpt-number'];
				$ret .= $blog_style->get_blog_excerpt($args);
			}
			$ret .= '</div>'; // gdlr-core-blog-modern-content
			$ret .= '</div>'; // gdlr-core-blog-modern-inner
			$ret .= '</div>'; // gdlr-core-blog-modern
			
			return $ret;

		}
	}