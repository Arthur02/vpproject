<?php

function donorsList() {
	ob_start();

    include(locate_template('/donors-section.php'));

    return ob_get_clean();
}

add_shortcode('donors-list', 'donorsList');

function donationAmount() {

    global $wpdb;

    $sql = "SELECT sum(donation_amount) as total_amount FROM wp_htz7kzeun4_frmaster_order AS fo";
    $donationAmount = $wpdb->get_results( $sql, 'ARRAY_A' );

    echo '<h3 class="chariti-widget-title">Total Donation Amount</h3>
    <p class="total-donation-amount">$'.$donationAmount[0]['total_amount'] . '</p>';

}

add_shortcode('donation-amount', 'donationAmount');

function googleForm() {
	ob_start();

    include(locate_template('/google-form.php'));

    return ob_get_clean();
}

add_shortcode('google-form', 'googleForm');



// SELECT * FROM wp_htz7kzeun4_frmaster_order AS fo 
// LEFT JOIN wp_htz7kzeun4_users AS users ON (fo.user_id = users.ID ) 
// LEFT JOIN wp_htz7kzeun4_posts as posts ON (fo.cause_id = posts.ID) 
// WHERE posts.post_type = 'cause' && posts.post_status = 'publish'