<form id="foundationCensus" action="">

<h1 style="margin-bottom: 35px;">AEO Foundation Census</h1>

<div class="tab">
  <p>
    <label for="">First Name<span>*</span></label>
    <input placeholder="Your answer" type="text" name="first-name" required oninput="this.className = ''">
  </p>
  <p>
    <label for="">Last Name<span>*</span></label>
    <input placeholder="Your answer" type="text" name="last-name" required oninput="this.className = ''">
  </p>
  <p>
    <label for="">Email<span>*</span></label>
    <input placeholder="Your answer" type="email" name="user-email" required oninput="this.className = ''">
  </p>
  <p>
    <label for="">Phone Number<span>*</span></label>
    <input placeholder="Your answer" type="text" name="phone-number" required oninput="this.className = ''">
  </p>
</div>

<div class="tab">
  <p>
    <label for="">Date of Birth (mm/dd/yyyy)<span>*</span></label>
    <input placeholder="Your answer" type="date" name="birthday" required oninput="this.className = ''">
  </p>
  <p>
    <label for="">Mailing Address<span>*</span></label>
    <input placeholder="Your answer" type="email" name="mailing-address" required oninput="this.className = ''">
  </p>
  <div>
    <label for="">Highest Level of Education Completed (Check ONE)<span>*</span></label>
    <p>
      <input placeholder="Your answer" type="radio" name="education" required oninput="this.className = ''">
      <label for="">Still in school</label>
    </p>
    <p>
      <input placeholder="Your answer" type="radio" name="education" required oninput="this.className = ''">
      <label for="">Associate degree (ex. AA, AS)</label>
    </p>
    <p>
      <input placeholder="Your answer" type="radio" name="education" required oninput="this.className = ''">
      <label for="">Bachelor's degree ( ex. BA, BS)</label>
    </p>
    <p>
      <input placeholder="Your answer" type="radio" name="education" required oninput="this.className = ''">
      <label for="">Master's degree (ex. MA, MS, MEng, MEd, MBA)</label>
    </p>
    <p>
      <input placeholder="Your answer" type="radio" name="education" required oninput="this.className = ''">
      <label for="">Professional/Doctoral degree (ex. CPA, MD, DDS, DVM, LLN, JD, PhD, EdD)</label>
    </p>
  </div>
  <p>
    <label for="">Major for highest level of education<span>*</span></label>
    <input placeholder="Your answer" type="text" name="major-level-education" required oninput="this.className = ''">
  </p>
  <p>
    <label for="">University attended for highest level of education<span>*</span></label>
    <input placeholder="Your answer" type="text" name="unversity-level-education" required oninput="this.className = ''">
  </p>
  <p>
    <label for="">Which chapter are you from?<span>*</span></label>
    <select name="chapter" oninput="this.className = ''">
      <option value="">Choose</option>
      <option value="Alpha - CSUN">Alpha - CSUN</option>
      <option value="Beta - UCLA">Beta - UCLA</option>
      <option value="Gamma - UCI">Gamma - UCI</option>
      <option value="Delta - CSULA">Delta - CSULA</option>
      <option value="Epsilon - USC">Epsilon - USC</option>
      <option value="Zeta - Cal Poly">Zeta - Cal Poly</option>
      <option value="Eta - UCSD">Eta - UCSD</option>
      <option value="Theta - UCR">Theta - UCR</option>
    </select>
  </p>
  <p>
    <label for="">When did you become a brother of AEO (ballpark)? (YYYY)<span>*</span></label>
    <input placeholder="Your answer" type="text" name="brother-aeo" required oninput="this.className = ''">
  </p>
  <div>
    <label for="">What is your AEO membership status? (check one) <span>*</span></label>
    <p>
      <input placeholder="Your answer" type="radio" name="membership-status" required oninput="this.className = ''">
      <label for="">Collegiate</label>
    </p>
    <p>
      <input placeholder="Your answer" type="radio" name="membership-status" required oninput="this.className = ''">
      <label for="">Alumni</label>
    </p>
  </div>
</div>

<div class="tab">
  <p>
    <label for="">What is your occupation?<span>*</span></label>
    <input placeholder="Your answer" type="text" name="occupation" required oninput="this.className = ''">
  </p>
  <p>
    <label for="">Who is your employer?<span>*</span></label>
    <input placeholder="Your answer" type="text" name="employer" required oninput="this.className = ''">
  </p>
  <div>
    <label for="">Are you interested in participating in the AEΩ mentorship program? (Your occupation and contact information will be shared with active AEΩ brothers with similar interests/career paths that might reach out to you for advice)<span>*</span></label>
    <p>
      <input placeholder="Your answer" type="radio" name="mentorship-program" required oninput="this.className = ''">
      <label for="">Yes</label>
    </p>
    <p>
      <input placeholder="Your answer" type="radio" name="mentorship-program" required oninput="this.className = ''">
      <label for="">No</label>
    </p>
    <p>
      <input placeholder="Your answer" type="radio" name="mentorship-program" required oninput="this.className = ''">
      <label for="">Maybe</label>
    </p>
  </div>  
  <div>
    <label for="">What AEP Foundation events are you interested in participating in? (choose as many options as apply)<span>*</span></label>
    <p>
      <input placeholder="Your answer" type="checkbox" name="events-family" required oninput="this.className = ''">
      <label for="">Family-friendly events (i.e. with spouse and/or children)</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="events-philanthropic" required oninput="this.className = ''">
      <label for="">Philanthropic</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="events-professional-networking" required oninput="this.className = ''">
      <label for="">Professional networking</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="events-raffles-and-auctions" required oninput="this.className = ''">
      <label for="">Raffles and auctions</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="events-social" required oninput="this.className = ''">
      <label for="">Social (card games, cigar/whiskey nights, restaurants)</label>
    </p>
  </div>  
  <p>
    <label for="">What AEO Foundation projects are you most interested in contributing your time and/or money?<span>*</span></label>
    <input placeholder="Your answer" type="text" name="aeo-projects" required oninput="this.className = ''">
  </p>
</div>

<div class="tab">
  <div>
    <label for="">Which categories of Armenia interests you the most? (select all that apply) <span>*</span></label>
    <p>
      <input placeholder="Your answer" type="checkbox" name="category-health" required oninput="this.className = ''">
      <label for="">Health</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="category-construction" required oninput="this.className = ''">
      <label for="">Construction</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="category-economy" required oninput="this.className = ''">
      <label for="">Economy</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="category-fundraising" required oninput="this.className = ''">
      <label for="">fundraising</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="category-communication" required oninput="this.className = ''">
      <label for="">Communication</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="category-education" required oninput="this.className = ''">
      <label for="">Education</label>
    </p>
  </div>
  <div>
    <label for="">I am able to help with... (choose as many options as apply) <span>*</span></label>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-conducting-research" required oninput="this.className = ''">
      <label for="">Conducting research</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-donating-goods" required oninput="this.className = ''">
      <label for="">Donating goods (specify in "Other")</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-financial-donations" required oninput="this.className = ''">
      <label for="">Financial donations</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-in-person-volunteering" required oninput="this.className = ''">
      <label for="">In-person volunteering</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-in-person-protesting" required oninput="this.className = ''">
      <label for="">In-person protesting</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-information-technology-services" required oninput="this.className = ''">
      <label for="">Information technology services</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-making-calls" required oninput="this.className = ''">
      <label for="">Making calls</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-photo-or-videos-services" required oninput="this.className = ''">
      <label for="">Photo/Video services</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-social-media-campaigns" required oninput="this.className = ''">
      <label for="">Social media campaigns</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-travel-to-armenia" required oninput="this.className = ''">
      <label for="">Travel to Armenia</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-writing-letter" required oninput="this.className = ''">
      <label for="">Writing letter</label>
    </p>
    <p>
      <input placeholder="Your answer" type="checkbox" name="help-other" oninput="this.className = ''">
      <label for="">Other:<input placeholder="Your answer" type="text" name="help-other-content" oninput="this.className = ''" style="margin-left: 12px; width: 300px;"></label>
    </p>
  </div>
    <p>
      <label for="">Do you have special skills or access to certain resources? If so, please list.<span>*</span></label>
      <input placeholder="Your answer" type="text" name="special-skills" required oninput="this.className = ''">
    </p>
    <p>
      <label for="">We would love to read your ideas about what we can do to continue fulfilling the AEO creed. Please share your thoughts about ideas, projects, initiatives, etc. that AEO Foundation should be involved with.<span>*</span></label>
      <input placeholder="Your answer" type="text" name="share-ideas" required oninput="this.className = ''">
    </p>

</div>

<div style="overflow:auto;">
  <div style="float:right;">
    <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
    <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
  </div>
</div>


<!-- Circles which indicates the steps of the form: -->
<div style="text-align:center;margin-top:40px;display: none;">
  <span class="step"></span>
  <span class="step"></span>
  <span class="step"></span>
  <span class="step"></span>
</div>


</form>



<script>

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}


</script>