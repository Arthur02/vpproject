<?php 

global $wpdb;

$sql = "SELECT * FROM wp_htz7kzeun4_frmaster_order AS fo";
$sql .= " LEFT JOIN wp_htz7kzeun4_users AS users ON (fo.user_id = users.ID ) ";
$sql .= " LEFT JOIN wp_htz7kzeun4_posts as posts ON (fo.cause_id = posts.ID)  ";
$sql .= " WHERE posts.post_type = 'cause' && posts.post_status = 'publish' ";
$donors = $wpdb->get_results( $sql, 'ARRAY_A' );

?>

<div style="margin-bottom: 20px;">
	<select name="project" id="project" style="height: 40px; padding: 0px 10px; border-radius: 4px; margin-right: 12px;">
		<option value="">Select Project</option>
		<?php foreach($donors as $project) { ?>
			<option value="<?php echo $project['post_name']; ?>"><?php echo $project['post_title']; ?></option>
		<?php } ?>
	</select>
	<select name="sort-amount" id="sortAmount" style="height: 40px; padding: 0px 10px; border-radius: 4px;">
		<option value="">Sort by donation amount</option>
		<option value="lower-higher">From lower to higher</option>
		<option value="higher-lower">From higher to lower</option>
	</select>
</div>

<table>

	<tr>
		<th>Donor</th>
		<th>Project</th>
		<th>Donation Amout</th>
	</tr>

    <?php foreach($donors as $donor) { ?>
		
		
			<tr data-amout="<?php echo $donor['donation_amount']; ?>" data-project="<?php echo $donor['post_name']; ?>">
				<td><?php echo $donor['display_name']; ?></td>
				<td><a href="<?php echo '/cause/' . $donor['post_name']; ?>"><?php echo $donor['post_title']; ?></a></td>
				<td class="donation-amout"><?php echo '$'.$donor['donation_amount']; ?></td>
			</tr>
		

	<?php } ?>

</table>


<script>

var found = {};
jQuery('#project option[value]').each(function(){
    if(found[jQuery(this).val()]){
        jQuery(this).remove();   
    }
    else{
         found[jQuery(this).val()] = true;   
    }
});

jQuery('#project').change(function(){
	$project = jQuery(this).val();
	if($project != '') {
		jQuery('table tr').hide();
	}
	jQuery('table tr').first().show();
	jQuery('table tr[data-project="' + $project + '"]').show();
});

var items = jQuery('tr');

jQuery('#sortAmount').change(function(){
	if(jQuery(this).val() == 'lower-higher'){
       sortAsc();
    }
    else if(jQuery(this).val() == 'higher-lower'){
        sortDesc();
    }
});

function sortAsc(){
    jQuery('table').empty();
    items.sort(function(a, b){
      return jQuery(a).attr('data-amout')-jQuery(b).attr('data-amout')
    });
    jQuery('table').append(items);
}
function sortDesc(){
    jQuery('table').empty();
    items.sort(function(a, b){
      return jQuery(b).attr('data-amout')-jQuery(a).attr('data-amout')
    });
    jQuery('table').append(items); 
}
    


</script>
